#!/bin/sh

# Imported from https://github.com/moparisthebest/static-curl/
# Minor modifications by: Martin Roukala <martin.roukala@mupuf.org>

# to test locally, run:
# docker run --rm -v $(pwd)/.gitlab-ci/curl-build.sh:/build.sh -w /tmp -e ARCH=amd64 alpine:latest /build.sh

CURL_VERSION='7.88.1'

[ "$1" != "" ] && CURL_VERSION="$1"

set -exu

# Verify that the binary does not current exist
URL="${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/curl/${CURL_VERSION}/curl-${ARCH}"
if wget --spider "$URL" 2>/dev/null; then
    echo "The binary already exists, abort!"
    exit 0
fi

if [ ! -f curl-${CURL_VERSION}.tar.gz ]
then

    # for gpg verification of the curl download below
#     apk add gnupg

    wget https://curl.haxx.se/download/curl-${CURL_VERSION}.tar.gz # https://curl.haxx.se/download/curl-${CURL_VERSION}.tar.gz.asc

#     # convert mykey.asc to a .pgp file to use in verification
#     gpg --no-default-keyring --yes -o ./curl.gpg --dearmor mykey.asc
#     # this has a non-zero exit code if it fails, which will halt the script
#     gpg --no-default-keyring --keyring ./curl.gpg --verify curl-${CURL_VERSION}.tar.gz.asc

fi

rm -rf "curl-${CURL_VERSION}/"
tar xzf curl-${CURL_VERSION}.tar.gz

cd curl-${CURL_VERSION}/

# dependencies to build curl
apk add build-base clang openssl-dev nghttp2-dev nghttp2-static libssh2-dev libssh2-static

# these are missing on at least armhf
apk add openssl-libs-static zlib-static || true

# gcc is apparantly incapable of building a static binary, even gcc -static helloworld.c ends up linked to libc, instead of solving, use clang
export CC=clang

# apply patches if needed
#patch -p1 < ../static.patch
#apk add autoconf automake libtool
#autoreconf -fi
# end apply patches

# set up any required curl options here
#LDFLAGS="-static" PKG_CONFIG="pkg-config --static" ./configure --disable-shared --enable-static --disable-libcurl-option --without-brotli --disable-manual --disable-unix-sockets --disable-dict --disable-file --disable-gopher --disable-imap --disable-smtp --disable-rtsp --disable-telnet --disable-tftp --disable-pop3 --without-zlib --disable-threaded-resolver --disable-ipv6 --disable-smb --disable-ntlm-wb --disable-tls-srp --disable-crypto-auth --without-ngtcp2 --without-nghttp2 --disable-ftp --disable-mqtt --disable-alt-svc --without-ssl

LDFLAGS="-static" PKG_CONFIG="pkg-config --static" ./configure --disable-shared --enable-static --disable-ldap --enable-ipv6 --enable-unix-sockets --with-ssl --with-libssh2

make -j4 V=1 LDFLAGS="-static -all-static"

# binary is ~13M before stripping, 2.6M after
strip src/curl

# print out some info about this, size, and to ensure it's actually fully static
ls -lah src/curl
file src/curl
# exit with error code 1 if the executable is dynamic, not static
ldd src/curl && exit 1 || true

./src/curl -V

# Upload the generated binary when merged in the main branch
if [ "${CI_COMMIT_BRANCH-premerge}" = "${CI_DEFAULT_BRANCH}" ]; then
    # running on the default branch. CI_COMMIT_BRANCH is *not* set in MRs
    ./src/curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}" --upload-file "./src/curl" "${URL}"
else
    echo -e "\n\nLet's just verify that we can load an HTTPS page"
    ./src/curl https://www.freedesktop.org
fi
